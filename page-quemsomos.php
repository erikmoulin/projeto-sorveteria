<?php 
//Template Name: Quem somos
?>

<?php get_header() ?>

    <img src="" alt="">
    <div>
        <h1><?php the_field('secao_principal_titulo'); ?></h1>
        <p><?php the_field('secao_principal_descricao'); ?></p>
    </div>

    <div>
        <div>
        <figure>
            <?php if( get_field('secao_principal_imagem') ): ?>
                <img src="<?php the_field('secao_principal_imagem'); ?>" />
            <?php endif; ?>
        </figure>
        <div>
        <figure>
            <?php if( get_field('sub_secao_1_imagem') ): ?>
                <img src="<?php the_field('sub_secao_1_imagem'); ?>" />
            <?php endif; ?>
        </figure>
            <p><?php the_field('sub_secao_1_texto'); ?></p>
        </div>
        <div>
        <figure>
            <?php if( get_field('sub_secao_2_imagem') ): ?>
                <img src="<?php the_field('sub_secao_2_imagem'); ?>" />
            <?php endif; ?>
        </figure>
            <p><?php the_field('sub_secao_2_texto'); ?></p>
        </div> 
        <div>
        <figure>
            <?php if( get_field('sub_secao_3_imagem') ): ?>
                <img src="<?php the_field('sub_secao_3_imagem'); ?>" />
            <?php endif; ?>
        </figure>
            <p><?php the_field('sub_secao_3_texto'); ?></p>
        </div> 
        <div>
        <figure>
            <?php if( get_field('sub_secao_4_imagem') ): ?>
                <img src="<?php the_field('sub_secao_4_imagem'); ?>" />
            <?php endif; ?>
        </figure>
            <p><?php the_field('sub_secao_4_texto'); ?></p>
        </div>
    </div>

    <p><?php the_field('secao_texto'); ?></p>

    <h1>BLOG</h1>

    <?php
        // Define our WP Query Parameters
        $the_query = new WP_Query( array('cat' => 8) );
    ?>

    <?php
        // Start our WP Query
        while ($the_query -> have_posts()) : $the_query -> the_post();
        // Display the Post Title with Hyperlink
    ?>
        <h1><?php the_field('titulo_blog'); ?></h1>
    <?php
        // Reapeat the process and reset once it hits the limit
        endwhile;
        wp_reset_postdata();
    ?>
    </section>

<?php get_footer() ?>