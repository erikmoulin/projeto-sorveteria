<?php
// Template Name: home
?>

<?php get_header(); ?>
    
    <div class="intro">
        <h1><?php the_field('titulo_inicial'); ?></h1>
        <p><?php the_field('descricao_inicial'); ?></p>
    </div>

    <div class="nossosProdutos">
        <h1 class="nossosProdutos">NOSSOS PRODUTOS</h1>
        <p>Tipos de Produtos:</p>
        <div class="produtos">
            <!-- Carrossel -->
        <script>
            var carrossel = [
                '<div class="tituloSorvete"><img src="<?php echo get_stylesheet_directory_uri() ?>/imagens/sorvete.png" alt=""><div class="gradiente"></div><div><p class="psorvete">Sorvete</p></div></div>',
                '<div class="tituloSorvete"><img src="<?php echo get_stylesheet_directory_uri() ?>/imagens/acai.png" alt=""><div class="gradiente"></div><div><p class="psorvete">Açaí</p></div></div>',
                '<div class="tituloSorvete"><img src="<?php echo get_stylesheet_directory_uri() ?>/imagens/picole.png" alt=""><div class="gradiente"></div><div><p class="psorvete">Picolé</p></div> </div>',
                '<div class="tituloSorvete"><img src="<?php echo get_stylesheet_directory_uri() ?>/imagens/quentes.png" alt=""><div class="gradiente"></div><div><p class="psorvete">Quentes</p></div></div>'
            ]


            function exibeCarrossel(contador1, contador2) {
                let loop = document.querySelector(".produtos")
                loop.innerHTML = carrossel[contador1] + carrossel[contador2]
            }

            exibeCarrossel(0, 1)

        </script>
        </div>
        <div>
            <span onclick="exibeCarrossel(0, 1)" class="dot">dot</span>
            <span onclick="exibeCarrossel(2, 3)" class="dot">dot</span>
        </div>
    </div>

    <!---->

    <div class="conhecaLoja">
        <h1><?php the_field('conhecer_nossa_loja_titulo'); ?></h1>
        <p><?php the_field('conheca_nossa_loja_descricao'); ?></p>
        <a href="">CONHEÇA MAIS</a>
    </div>

<?php get_footer(); ?> 