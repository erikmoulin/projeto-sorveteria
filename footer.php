<footer class="footer">
        <div class="horarios">
            <p>Horário de Funcionamento</p>
            <p>Segunda-Feira: <?php the_field('horario_de_funcionamento_segunda-feira'); ?></p>
            <p>Terça-Feira: <?php the_field('horario_de_funcionamento_terca-feira'); ?></p>
            <p>Quarta-Feira: <?php the_field('horario_de_funcionamento_quarta-feira'); ?></p>
            <p>Quinta-Feira: <?php the_field('horario_de_funcionamento_quinta-feita'); ?></p>
            <p>Sexta-Feira: <?php the_field('horario_de_funcionamento_sexta-feira'); ?></p>
            <p>Sábado: <?php the_field('horario_de_funcionamento_sabado'); ?></p>
            <p>Domingo: <?php the_field('horario_de_funcionamento_domingo'); ?></p>
        </div>
        
        <div class="iconesFooter">
            <a href="<?php the_field('instagram_icone'); ?>"><img src="" alt="insta"></a>
            <p><?php the_field('instagram_@url'); ?></p>
            <a href="<?php the_field('whatsapp_icone'); ?>"><img src="" alt="whatsapp"></a>
            <p><?php the_field('whatsapp_num_url'); ?></p>
        </div>

        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3677.4672976410934!2d-43.050075199999995!3d-22.822194599999996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x999afa5c267e07%3A0x872e3028a4506a26!2sR.%20Carlos%20Gianelli%2C%20413%20-%20Boa%C3%A7u%2C%20S%C3%A3o%20Gon%C3%A7alo%20-%20RJ%2C%2024465-000!5e0!3m2!1spt-BR!2sbr!4v1663177773600!5m2!1spt-BR!2sbr" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </footer>
        <p class="copyright">COPYRIGHT © PROJETO 2022 - TODOS OS DIREITOS RESERVADOS</p>
</body>
</html>