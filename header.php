<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>inicio</title>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
</head>
<body>

    <header class="header">
        <img src="<?php echo get_stylesheet_directory_uri() ?>/imagens/logo.png" alt="LOGO">
        <a href="/produtos.php">Produtos</a>
        <a href="/page-quemsomos.php">Quem Somos</a>
        <h1>|</h1>
        <a href="<?php the_field('instagram_icone'); ?>"><img src="" alt="instagram"></a>
        <a href="<?php the_field('whatsapp_icone'); ?>"><img src="" alt="whatsapp"></a>
    </header>